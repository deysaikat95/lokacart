package com.mobile.ict.cart.util;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Base64;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.container.Product;
import com.mobile.ict.cart.container.ProductType;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.database.DBHelper;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

/**
 * Created by Vishesh on 16-03-2016.
 */

public class Master {

    public static final String serverURL = "http://ruralict.cse.iitb.ac.in/ruralict/";

    public static String getLoginURL()
    {
        return serverURL + "app/login";
    }
    public static String getForgotPasswordURL()
    {   //" http://ruralict.cse.iitb.ac.in/RuralIvrs/app/forgotpassword";
        return serverURL + "app/forgotpassword";
    }
    public static String getChangePasswordURL()
    {
        return serverURL + "app/changepassword";
    }
    public static String getDetailsFormURL(){
        return serverURL + "app/loginform";
    }
    public static String getEnquiryFormURL ()
    {
        return serverURL + "app/enquiry";
    }
    public static String getEditProfileURL ()
    {
        return serverURL + "api/Test2/manageUsers/editProfile";
    }
    public static String getChangeProfilePasswordURL ()
    {
        return serverURL + "app/changepassword";
    }

    public static String getNumberVerifyURL ()
    {
        return serverURL + "app/numberverify";
    }
    public static String getChangeNumberURL ()
    {
        return serverURL + "app/changenumber";
    }
    public static String getLeaveOrganisationURL()
    {
        return serverURL + "app/delete";
    }
    public static String getSendReferralURL()
    {
        return serverURL + "api/refer";
    }
    public static String getProductsURL(String orgAbbr)
    {
        return serverURL + "api/products/search/byType/mapnew?orgabbr=" + orgAbbr;
    }
    public static String getProductTypesURL(String orgAbbr)
    {
        return serverURL + "api/gettypes?abbr=" + orgAbbr;
    }
    public static String getPlacingOrderURL()
    {
        return serverURL + "api/orders/add";
    }
    public static String getEditPlacedOrderURL(int orderID)
    {
        return serverURL + "api/orders/update/"+orderID;
    }

    public static String getOrgInfoURL(String orgAbbr)
    {
        return serverURL + "api/"+orgAbbr+"/orginfo";
    }

    public static String getPlacedOrderURL(String orgAbbr,String mobileNumber)
    {
        return serverURL + "api/savedorders?abbr="+orgAbbr+"&phonenumber=91"+mobileNumber;
    }
    public static String getCancellingOrderURL(String orderID)
    {
        return serverURL + "api/orders/update/"+orderID;
    }
    public static String getProcessedOrderURL(String orgAbbr,String mobileNumber)
    {
        // return serverURL + "api/orders/search/getOrdersForMember?format=binary&status=processed&abbr="+orgAbbr+"&phonenumber=91"+mobileNumber+"&projection=default";
        return serverURL + "api/processedorders?abbr=" + orgAbbr + "&phonenumber=91" + mobileNumber;
    }


    public static String getRegisteredUserMobileVerifyURL ()
    {
        return serverURL + "app/recoverotp";
    }

    public static String getRegisteredUserDetailsURL ()
    {
        return serverURL + "app/recoverdetails";
    }
    //the difference bw this and old version check api is that if we provide the mobile number in the api call
    //the response will also contain the flag if all user details are present in server or not
    public static String getVersionCheckNewURL()
    {
        return serverURL + "app/versionchecknew";
    }



    public static String getSendCommentURL()
    {
        return serverURL + "api/feedback";
    }

    public static String getSendReferURL()
    {
        return serverURL + "/api/refer";
    }

    public static String sendGCMTokenUrl()
    {
        return serverURL + "/app/registertoken";
    }

    public static String verifyCartURL()
    {
        return serverURL + "api/cartUpdate";
    }
    public static  String getAcceptReferralURL(){
        return serverURL+"app/acceptreferral";
    }


    public static String getUploadImageURL(String number){

       return serverURL+ "/api/profpicupload?phonenumber="+ "91"+number;
    }

    public static final String UpdateRequiredPref = "updateRequired";

    public static final String showAddToCartDialogPref = "showaddtocartdialog";

    public static final String AcceptReferralFailedPref = "acceptReferralFailed";

    public static final String ReferralCodePref = "referralCode";

    public static final String PRODUCT_DRAWER_ALERT_TAG = "productDrawer";

    public static final String VERSION_CHECK_NEW_PHONE_NUMBER_TAG = "phonenumber";
    public static final String VERSION_CHECK_NEW_VERSION_TAG = "version";

    //either  0,1 or 2
    public static final String VERSION_CHECK_NEW_RESPONSE_UPDATE_REQUIRED_TAG = "update";
    public static final String VERSION_CHECK_NEW_RESPONSE_PINCODE_TAG = "pincode";
    public static final String VERSION_CHECK_NEW_RESPONSE_ADDRESSS_TAG = "address";

    // 0 - all details present, 1- details missing
    public static final String VERSION_CHECK_NEW_RESPONSE_FLAG_TAG = "flag";
    public static final String VERSION_CHECK_NEW_RESPONSE_FNAME_TAG = "name";
    public static final String VERSION_CHECK_NEW_RESPONSE_EMAIL_TAG = "email";
    public static final String VERSION_CHECK_NEW_RESPONSE_LNAME_TAG = "lastname";


    // tags to get data from the json response of accept referral async task
    //activity used in -> splash screen activity
    public static final String ACCEPT_REFERRAL_RESPONSE_TAG = "response";
    public static final String ACCEPT_REFERRAL_ORGANISATION_REFERRED_TO_TAG = "organization";
    public static final String ACCEPT_REFERRAL_ORGANISATION_REFERRED_ABBR_TO_TAG = "abbr";
    public static final String ACCEPT_REFERRAL_USER_NAME_TAG = "name";
    public static final String ACCEPT_REFERRAL_USER_PROFILE_PIC_URL = "profilepic";
    public static final String ACCEPT_REFERRAL_USER_EMAIL_TAG = "email";
    public static final String ACCEPT_REFERRAL_USER_LAST_NAME_TAG = "lastname";
    public static final String ACCEPT_REFERRAL_PINCODE_TAG = "pincode";
    public static final String ACCEPT_REFERRAL_USER_NUMBER_TAG = "phonenumber";
    public static final String ACCEPT_REFERRAL_ORGANISATION_LIST_TAG = "orglist";
    public static final String ACCEPT_REFERRAL_USER_ADDRESS_TAG = "address";
    public static final String ACCEPT_REFERRAL_APP_VERSION_TAG = "version";
    public static final String ACCEPT_REFERRAL_UPDATE_MANDATORY_TAG = "update";
    public static final String ACCEPT_REFERRAL_DETAILS_PRESENT = "flag";




    public static final String IMAGE_FILE_TYPE = "image/jpg";

    //---------------- fragment tags ---------------------------

    public static final String
                        PRODUCT_TAG = "product_fragment",
                        PLACED_ORDER_TAG = "placed_order_fragment",
                        PROCESSED_ORDER_TAG = "processed_order_fragment",
                        PROFILE_TAG = "profile_fragment",
                        REFERRALS_TAG = "referrals_fragment",
                        CHANGE_ORGANISATION_TAG = "organisation_fragment",
                        FEEDBACKS_TAG = "feedbacks_fragment",
                        CONTACT_US_TAG = "contact_us_fragment",
                        TERMS_AND_CONDITIONS_TAG = "terms_and_conditions_fragment",
                        FAQ_TAG = "faq_fragment",
                        ABOUT_US_TAG = "about_us_fragment";
    //----------------------------------------------------------

    //---------------- Navigation drawer textviews -------------

    public static TextView drawerName, drawerEmail, drawerMobileNumber;

    //----------------------------------------------------------

    //---------------- navigation drawer item keys -------------
    public static final int
                        PRODUCT_KEY = 0,
                        PLACED_ORDER_KEY = 1,
                        PROCESSED_ORDER_KEY = 2,
                        PROFILE_KEY = 3,
                        REFERRALS_KEY = 4,
                        CHANGE_ORGANISATION_KEY = 5,
                        LOGOUT_KEY = 6,
                        FEEDBACKS_KEY = 7,
                        CONTACT_US_KEY = 8,
                        TERMS_AND_CONDITIONS_KEY = 9,
                        FAQ_KEY = 10;


    public static int CART_ITEM_COUNT=0;

    //---------------------------------------------------------

    //---------------- SQL DB Keys ------------------------------

    public static final String
                    PRODUCT_NAME = "pname",
                    PRICE = "price",
                    TOTAL = "total",
                    QUANTITY = "quantity",
                    ID = "id",
                    IMAGE_URL = "imageurl",
                    AUDIO_URL ="audiourl";

    public static final String
                    STOCK_QUANTITY = "stockquantity";

    //-----------------------------------------------------------

    //---------------- shared preference keys-------------------

    public static final String
                        FNAME = "firstname",
                        LNAME = "lastname",
                        EMAIL = "email",
                        ADDRESS = "address",
                        MOBILENUMBER = "phonenumber",
                        PASSWORD = "password",
                        PINCODE = "pincode",
                        STEPPER = "stepper",
                        INTRO = "intro",
                        SIGNCHK = "sign_check",
                        RESPONSE = "response",
                        STATUS =  "status",
                        ORGANISATIONS = "organizations",
                        ORG_NAME = "name",
                        ORG_ABBR = "abbr",
                        ORG_CONTACT = "contact",
                        SELECTED_ORG_NAME = "selectedOrgName",
                        SELECTED_ORG_ABBR = "selectedOrgAbbr",
                        REFER_ORG_ABBR = "reforgabbr",
            STOCK_MANAGEMENT_STATUS = "stockManagement",
                        REFER_EMAIL = "refemail";

    public static final String feedbackText = "content";


    public static final String
            DEFAULT_FNAME = "",
            DEFAULT_LNAME = "",
            DEFAULT_EMAIL = "",
            DEFAULT_ADDRESS = "",
            DEFAULT_MOBILENUMBER = "",
            DEFAULT_PASSWORD = "",
            DEFAULT_PINCODE = "",
            DEFAULT_LOGIN_JSON = "",
            DEFAULT_ORG_NAME = "null",
            DEFAULT_ORG_ABBR = "abbr";

    public static Boolean
            DEFAULT_SIGNCHK = false,
            DEFAULT_STEPPER = true,
            DEFAULT_INTRO = true;

    public static final String
            LOGIN = "login",
            LOGIN_JSON = "loginJSON";

    public static String
          PLACEDORDER = "placed",
          PROCESSEDORDER = "processed";



    public static void initialise(Context context)
    {
        Master.getJSON = new GetJSON();
        Master.responseObject = new JSONObject();
        Master.getMemberDetails(context);
    }
    //----------------------------------------------------------

    //------- GCM ---------------------

    public static String token;

    public static String getToken()
    {
        return token;
    }
    public static void setToken(String tok)
    {
        token = tok;
    }

    //--------------------------------

    public static boolean isNetworkAvailable(Context context)
    {
        ConnectivityManager connectivityManager;
        connectivityManager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return (activeNetworkInfo != null && activeNetworkInfo.isConnected());
    }

    //-----------------------------------------------------------

    //------------public objects---------------------------------

    public static boolean isMember; // to handle if user is no longer a member
    public static boolean isProductClicked; // to handle multiple clicks in recycler view
    public static GetJSON getJSON;
    public static String response; //for storing JSON in AsyncTasks
    public static JSONObject responseObject; //for making JSONObject from response;
   // public static DBHelper dbHelper;

    public static ArrayList<Product> productList;
    public static ArrayList<Product> cartList,editOrderList;
    public static ArrayList<ProductType> productTypeList;
    public static ArrayList<Integer> changeCheckList;

    //-----------------------------------------------------------

    public static void getMemberDetails(Context context)
    {

        DBHelper dbHelper = new DBHelper(context);
        dbHelper.getSignedInProfile();
        /*
        MemberDetails.setEmail(SharedPreferenceConnector.readString(context, Master.EMAIL, Master.DEFAULT_EMAIL));
        MemberDetails.setMobileNumber(SharedPreferenceConnector.readString(context, Master.MOBILENUMBER, Master.DEFAULT_MOBILENUMBER));
        MemberDetails.setPassword(SharedPreferenceConnector.readString(context, Master.PASSWORD, Master.DEFAULT_PASSWORD));
        MemberDetails.setFname(SharedPreferenceConnector.readString(context, Master.FNAME, Master.DEFAULT_FNAME));
        MemberDetails.setLname(SharedPreferenceConnector.readString(context, Master.LNAME, Master.DEFAULT_LNAME));
        MemberDetails.setAddress(SharedPreferenceConnector.readString(context, Master.ADDRESS, Master.DEFAULT_ADDRESS));
        MemberDetails.setPincode(SharedPreferenceConnector.readString(context, Master.PINCODE, Master.DEFAULT_PINCODE));*/
    }

    /**
     * Hides the soft keyboard
     */
    public static void hideSoftKeyboard(Context context) {
        if(((Activity)context).getCurrentFocus() !=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(((Activity)context).getCurrentFocus().getWindowToken(), 0);
        }
    }

    //----------------function to clear a bitmap from memory---------------------------------
    //---------------------------------------------------------------------------------------
    public static void clearBitmap(Bitmap bm) {
        bm.recycle();
        System.gc();
    }
    //***************************************************************************************


    public static String okhttpUpload(File file, String serverURL, String fileType, String email, String password) {

        OkHttpClient client = new OkHttpClient();
//        File file= new File (path);
        JSONObject jsonObject;


        RequestBody requestBody = new MultipartBuilder()
                .type(MultipartBuilder.FORM)
                .addFormDataPart("file", file.getName(),
                        RequestBody.create(MediaType.parse(fileType), file))
                .build();

        Request request = new Request.Builder()
                .url(serverURL)
                .post(requestBody)
                .addHeader("authorization", "Basic " + new String(Base64.encode((email + ":" + password).getBytes(), Base64.NO_WRAP)))
                .build();

        try
        {
            Response response = client.newCall(request).execute();
            String resp = response.body().string();
            return resp;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (Exception ex)
        {
            return null;
        }
    }


    public static void copyFile(File src, File dst) throws IOException {
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();

    }


    public static void setBadgeCount(Context context, LayerDrawable icon, int count) {

        if (Build.VERSION.SDK_INT <= 15) {
            return;
        }

        CartIconDrawable badge;

        //LayerDrawable d = (LayerDrawable)icon.getDrawable(0);
        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof CartIconDrawable) {
            badge = (CartIconDrawable) reuse;
        } else {
            badge = new CartIconDrawable(context);
        }
      //  badge = new CartIconDrawable(context);
        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
       // icon.invalidateSelf();
    }

   /* public static  MenuItem cartItem;
    public static  LayerDrawable localLayerDrawable;
    public static  Drawable cartBadgeDrawable;
    public static CartIconDrawable badgeDrawable ;

    public static void createCartBadge(Context context,Menu menu,int paramInt) {
       *//* if (Build.VERSION.SDK_INT <= 15) {
            return;
        }*//*
        cartItem = menu.findItem(R.id.action_cart);
       // if(cartItem.getIcon()!=null)
       // {
            localLayerDrawable = (LayerDrawable) cartItem.getIcon();
            cartBadgeDrawable = localLayerDrawable.findDrawableByLayerId(R.id.ic_badge);
       // }
        badgeDrawable = new CartIconDrawable(context);
      //  badgeDrawable = (CartIconDrawable) cartBadgeDrawable;

       *//* if ((cartBadgeDrawable != null)
                && ((cartBadgeDrawable instanceof CartIconDrawable))
               ) {
            badgeDrawable = (CartIconDrawable) cartBadgeDrawable;
        } else {
            badgeDrawable = new CartIconDrawable(context);
        }*//*
        badgeDrawable.setCount(paramInt);
        localLayerDrawable.mutate();
        localLayerDrawable.setDrawableByLayerId(R.id.ic_badge, badgeDrawable);
        //cartItem.setIcon(localLayerDrawable);
    }*/

    public static void updateProductList()
    {
        for(int i = 0; i < Master.cartList.size(); ++i)
        {
            for(int j = 0; j< Master.productList.size(); ++j)
            {
                if(Master.productList.get(j).getID().equals(Master.cartList.get(i).getID()))
                {
                    Master.productList.get(j).setQuantity(Master.cartList.get(i).getQuantity());
                    Master.productList.get(j).setUnitPrice(Master.cartList.get(i).getUnitPrice());
                    break;
                }
            }
        }
    }




}
