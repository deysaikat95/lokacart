package com.mobile.ict.cart;

import android.app.Application;
import android.util.Log;

import com.flurry.android.FlurryAgent;
import com.mikepenz.iconics.utils.Utils;

import io.branch.referral.Branch;

/**
 * Created by sid on 26/5/16.
 */

public class LokacartApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Branch.getAutoInstance(this);
        new FlurryAgent.Builder()
                .withLogEnabled(true)
                .withLogEnabled(true)
                .withLogLevel(Log.DEBUG)
                .build(this, getString(R.string.flurry_key));

        FlurryAgent.init(this,getString(R.string.flurry_key));

    }
}

