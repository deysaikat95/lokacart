package com.mobile.ict.cart.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alertdialogpro.AlertDialogPro;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.container.Product;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.activity.DashboardActivity;
import com.mobile.ict.cart.adapter.ProductAdapter;
import com.mobile.ict.cart.database.DBHelper;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;
import com.mobile.ict.cart.container.ProductType;
import com.mobile.ict.cart.util.SharedPreferenceConnector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by vish on 21/3/16.
 */
public class ProductFragment extends Fragment {

    View productFragmentView;
    RelativeLayout relativeLayout;
    Drawer result;
    ProductType productType;
    Product product;
     String stockEnabledStatus="false";
    RecyclerView recyclerView;
    DrawerBuilder drawerBuilder;
    ProductAdapter swapAdapter;
    StaggeredGridLayoutManager layoutManager;
    DBHelper dbHelper;
    ImageView ivNoProduct, ivNoData;
    TextView tNoProduct, tNoData;
    Menu searchMenu;
    boolean isInternet =true;

private String TAG ="ProductFragment";
    public static int selectedProductTypePosition = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        setHasOptionsMenu(true);
        if(!Master.isNetworkAvailable(getActivity()))
        {
            productFragmentView = inflater.inflate(R.layout.no_internet_layout, container, false);
            getActivity().setTitle(R.string.menu_products);
            isInternet = false;
            setHasOptionsMenu(true);
        }
        else {

            isInternet = true;
            selectedProductTypePosition = 0;
            Master.isProductClicked = false;
            productFragmentView = inflater.inflate(R.layout.fragment_products, container, false);

            dbHelper = new DBHelper(getActivity());

            getActivity().setTitle(R.string.title_fragment_product);
            relativeLayout = (RelativeLayout) productFragmentView.findViewById(R.id.relativeLayout);

            layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);

            recyclerView = (RecyclerView) productFragmentView.findViewById(R.id.rvProduct);
            recyclerView.setHasFixedSize(true);

            recyclerView.setLayoutManager(layoutManager);

            recyclerView.setMotionEventSplittingEnabled(false);

            //recyclerView.setEnabled(false);
            ivNoProduct = (ImageView) productFragmentView.findViewById(R.id.ivNoProduct);
            tNoProduct = (TextView) productFragmentView.findViewById(R.id.tNoProduct);

            ivNoProduct.setVisibility(View.GONE);
            tNoProduct.setVisibility(View.GONE);

            ivNoData = (ImageView) productFragmentView.findViewById(R.id.ivNoData);
            tNoData = (TextView) productFragmentView.findViewById(R.id.tNoData);

            ivNoData.setVisibility(View.GONE);
            tNoData.setVisibility(View.GONE);

            drawerBuilder = new DrawerBuilder();

            Master.cartList = new ArrayList<>();

            setHasOptionsMenu(true);

            new GetProductsTask(savedInstanceState).execute();
        }

        return productFragmentView;
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        searchMenu = menu;

        /*if(!Master.isNetworkAvailable(getActivity()))
            menu.clear();*/

        if(!isInternet)
            menu.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
        Master.getMemberDetails(getActivity());

        /*Master.productList = productTypes.get(selectedProductTypePosition).productItems;
        Master.cartList = dbHelper.getCartDetails(MemberDetails.getMobileNumber(), MemberDetails.getSelectedOrgAbbr());
        updateProductList();*/

        Master.isProductClicked = false;
//        Log.e("Product frag", "in OnResume");

        if(recyclerView != null)
        {
            isInternet = true;
            ProductAdapter rcAdapter = new ProductAdapter(getActivity());
            recyclerView.swapAdapter(rcAdapter, false);

            if(Master.productTypeList != null && !Master.productTypeList.isEmpty())
                updateCart();

        }
        else
        {
            isInternet = false;
        }

        if (result != null)
            result.setSelectionAtPosition(selectedProductTypePosition);

        if(!isInternet && searchMenu != null)
            searchMenu.clear();

        getActivity().invalidateOptionsMenu();

    }

    public class GetProductsTask extends AsyncTask<Void, String, String> {

        Bundle savedInstanceState;

        public GetProductsTask(Bundle savedInstanceState) {
            this.savedInstanceState = savedInstanceState;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Material.circularProgressDialog(getActivity(), getString(R.string.pd_fetching_products), true);
        }

        @Override
        protected String doInBackground(Void... params) {

            Master.getJSON = new GetJSON();
            String url = Master.getProductsURL(dbHelper.getSelectedOrg(MemberDetails.getMobileNumber())[0]);

            dbHelper.getTable();
//            System.out.println("-- Mobile number: " + MemberDetails.getMobileNumber());

//            System.out.println("URL: " + url);
            Master.response = Master.getJSON.getJSONFromUrl(url, null, "GET", true,MemberDetails.getEmail(), MemberDetails.getPassword());
//            System.out.println(Master.response);
            return Master.response;
        }

        @Override
        protected void onPostExecute(String response)
        {
            if(Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();

            if(ProductFragment.this.isAdded())
            {
                if(!response.equals("exception"))
                {
                    try {
                        JSONObject object = new JSONObject(response);
                        JSONArray array = object.getJSONArray("products");
                        Master.productTypeList = new ArrayList<>();
                        if (array.length() > 0) {
                            for (int x = 0; x < array.length(); ++x) {
                                JSONObject prodTypeObj = array.getJSONObject(x);
                                Iterator<String> keysItr = prodTypeObj.keys();
                                int count=0;
                                while (keysItr.hasNext()) {
//                                    Log.d(TAG, String.valueOf(count++));
                                    String key = keysItr.next();

//                                    Log.d(TAG,key);
                                    productType = new ProductType(key);
                                    Object category = prodTypeObj.get(key);
//                                    System.out.println("Product type: " + productType.getName());
                                    if (category instanceof JSONArray) {
                                        for (int i = 0; i < ((JSONArray) category).length(); i++) {

                                            if (((JSONArray) category).getJSONObject(i).has("imageUrl") && ((JSONArray) category).getJSONObject(i).has("audioUrl")) {
                                                if (((JSONArray) category).getJSONObject(i).getString("imageUrl") == null && ((JSONArray) category).getJSONObject(i).getString("audioUrl") == null) {
                                                    product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                            Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                            0.0,
                                                            Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                            ((JSONArray) category).getJSONObject(i).getString("stockManagement"), "null", "null",
                                                            ((JSONArray) category).getJSONObject(i).getString("id"),
                                                            ((JSONArray) category).getJSONObject(i).getString("description"));
                                                } else if (((JSONArray) category).getJSONObject(i).getString("imageUrl") == null) {
                                                    product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                            Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                            0.0,
                                                            Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                            ((JSONArray) category).getJSONObject(i).getString("stockManagement"),
                                                            "null",
                                                            ((JSONArray) category).getJSONObject(i).getString("audioUrl"),
                                                            ((JSONArray) category).getJSONObject(i).getString("id"),
                                                            ((JSONArray) category).getJSONObject(i).getString("description"));
                                                } else if (((JSONArray) category).getJSONObject(i).getString("audioUrl") == null) {
                                                    product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                            Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                            0.0,
                                                            Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                            ((JSONArray) category).getJSONObject(i).getString("stockManagement"),
                                                            ((JSONArray) category).getJSONObject(i).getString("imageUrl"),
                                                            "null",
                                                            ((JSONArray) category).getJSONObject(i).getString("id"),
                                                            ((JSONArray) category).getJSONObject(i).getString("description"));
                                                } else {
                                                    product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                            Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                            0.0,
                                                            Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                            ((JSONArray) category).getJSONObject(i).getString("stockManagement"),
                                                            ((JSONArray) category).getJSONObject(i).getString("imageUrl"),
                                                            ((JSONArray) category).getJSONObject(i).getString("audioUrl"),
                                                            ((JSONArray) category).getJSONObject(i).getString("id"),
                                                            ((JSONArray) category).getJSONObject(i).getString("description"));
                                                }
                                            } else if (((JSONArray) category).getJSONObject(i).has("imageUrl")) {
                                                if (((JSONArray) category).getJSONObject(i).getString("imageUrl") == null) {
                                                    product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                            Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                            0.0,
                                                            Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                            ((JSONArray) category).getJSONObject(i).getString("stockManagement"),
                                                            "null",
                                                            "null",
                                                            ((JSONArray) category).getJSONObject(i).getString("id"),
                                                            ((JSONArray) category).getJSONObject(i).getString("description"));
                                                } else {
                                                    product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                            Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                            0.0,
                                                            Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                            ((JSONArray) category).getJSONObject(i).getString("stockManagement"),
                                                            ((JSONArray) category).getJSONObject(i).getString("imageUrl"),
                                                            "null",
                                                            ((JSONArray) category).getJSONObject(i).getString("id"),
                                                            ((JSONArray) category).getJSONObject(i).getString("description"));
                                                }
                                            } else if (((JSONArray) category).getJSONObject(i).has("audioUrl")) {
                                                if (((JSONArray) category).getJSONObject(i).getString("audioUrl") == null) {
                                                    product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                            Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                            0.0,
                                                            Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                            ((JSONArray) category).getJSONObject(i).getString("stockManagement"),
                                                            "null",
                                                            "null",
                                                            ((JSONArray) category).getJSONObject(i).getString("id"),
                                                            ((JSONArray) category).getJSONObject(i).getString("description"));
                                                } else {
                                                    product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                            Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                            0.0,
                                                            Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                            ((JSONArray) category).getJSONObject(i).getString("stockManagement"),
                                                            "null",
                                                            ((JSONArray) category).getJSONObject(i).getString("audioUrl"),
                                                            ((JSONArray) category).getJSONObject(i).getString("id"),
                                                            ((JSONArray) category).getJSONObject(i).getString("description"));
                                                }
                                            } else {
                                                product = new Product(((JSONArray) category).getJSONObject(i).getString("name"),
                                                        Double.parseDouble(((JSONArray) category).getJSONObject(i).getString("unitRate")),
                                                        0.0,
                                                        Integer.parseInt(((JSONArray) category).getJSONObject(i).getString("quantity")),
                                                        ((JSONArray) category).getJSONObject(i).getString("stockManagement"),
                                                        "null",
                                                        "null",
                                                        ((JSONArray) category).getJSONObject(i).getString("id"),
                                                        ((JSONArray) category).getJSONObject(i).getString("description"));
                                            }

                                            stockEnabledStatus = ((JSONArray) category).getJSONObject(i).getString("stockManagement");

                                            productType.productItems.add(product);
                                        }
                                    }

                                    int i = 0; // to set the selection dynamically afterwards
                                    if (productType.productItems.size() > 0) {
                                        drawerBuilder.addDrawerItems(new PrimaryDrawerItem().withName(productType.getName()).withIdentifier(i++));
//                                        Log.e("Product frg", productType.getName() + " added to drawer");
                                        Master.productTypeList.add(productType);
                                    }
                                }
                            }
                                DashboardActivity.updateSearchAdapter();
                                //setting up the navigation drawer
//                                System.out.println("setting up the navigation drawer");

                                result = drawerBuilder
                                        .withActivity(getActivity())
                                        .withRootView(relativeLayout)
                                        .withDisplayBelowStatusBar(false)
                                        .withSavedInstance(savedInstanceState)
                                        .withDrawerGravity(Gravity.END)
                                        .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {

                                            @Override

                                            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {

                                                drawerItemClick(position);
                                                return true;
                                            }

                                        })
                                        .buildForFragment();


                                result.getDrawerLayout().setFitsSystemWindows(true);
                                result.getSlider().setFitsSystemWindows(true);
                                Master.productList = Master.productTypeList.get(0).productItems;
                                Master.cartList = dbHelper.getCartDetails(MemberDetails.getMobileNumber(), MemberDetails.getSelectedOrgAbbr());
                                updateCart();
                                Master.updateProductList();

                                ProductAdapter rcAdapter = new ProductAdapter(getActivity());
                                recyclerView.setAdapter(rcAdapter);
                                getActivity().setTitle(Master.productTypeList.get(0).getName());

                                if(SharedPreferenceConnector.readBoolean(getActivity(), Master.PRODUCT_DRAWER_ALERT_TAG, false) == false)
                                {
                                    AlertDialogPro.Builder builder = new AlertDialogPro.Builder(getActivity());
                                    builder.setCancelable(false);
                                    builder.setMessage(getActivity().getString(R.string.alert_right_drawer_alert));
                                    builder.setPositiveButton(R.string.button_dont_show_again, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            SharedPreferenceConnector.writeBoolean(getActivity(), Master.PRODUCT_DRAWER_ALERT_TAG, true);
                                            result.openDrawer();
                                        }
                                    });
                                    builder.setNegativeButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            //SharedPreferenceConnector.writeBoolean(getActivity(), Master.PRODUCT_DRAWER_ALERT_TAG, false);
                                            result.openDrawer();
                                        }
                                    });
                                    builder.show();
                                }
                            }


                        else
                        {
                            //Material.alertDialog(getActivity(), getString(R.string.alert_no_products), "OK");
                            recyclerView.setVisibility(View.GONE);
                            ivNoProduct.setVisibility(View.VISIBLE);
                            tNoProduct.setVisibility(View.VISIBLE);

                            if(searchMenu != null)
                                searchMenu.clear();
                        }
                    }
                    catch (JSONException e)
                    {
//                        Log.e("Product frag", "Inside catch. " + e.getMessage());
                    }
                }
                else
                {
                    // if exception
                    recyclerView.setVisibility(View.GONE);
                    ivNoData.setVisibility(View.VISIBLE);
                    tNoData.setVisibility(View.VISIBLE);
                    searchMenu.clear();
                }
            }
        }
    }

    public void setSelection(int position)
    {
//        Log.e("Product frag", "in setSelection position: " + position);
        selectedProductTypePosition = position;
    }

    public void drawerItemClick(int position)
    {
        result.closeDrawer();
//        Log.e("Product frag", "Drawer clicked: " + position);

        selectedProductTypePosition = position;
        Master.productList = Master.productTypeList.get(position).productItems;

        Master.updateProductList();

        swapAdapter = new ProductAdapter(getActivity());
        recyclerView.swapAdapter(swapAdapter, false);
        getActivity().setTitle(Master.productTypeList.get(position).getName());
    }

    public void updateCart()
    {
        for(int i = 0; i < Master.productTypeList.size(); ++i)
        {
            for (int j = 0; j < Master.productTypeList.get(i).productItems.size(); ++j)
            {
                for(int z=0; z < Master.cartList.size();z++)
                {
                    if(Master.cartList.get(z).getID().equals(Master.productTypeList.get(i).productItems.get(j).getID()))
                    {
                        Master.cartList.get(z).setName(Master.productTypeList.get(i).productItems.get(j).getName());
                        Master.cartList.get(z).setUnitPrice(Master.productTypeList.get(i).productItems.get(j).getUnitPrice());
                        Master.cartList.get(z).setImageUrl(Master.productTypeList.get(i).productItems.get(j).getImageUrl());
                        Master.cartList.get(z).setStockQuantity(Master.productTypeList.get(i).productItems.get(j).getStockQuantity());
                        Master.cartList.get(z).setStockEnabledStatus(Master.productTypeList.get(i).productItems.get(j).getStockEnabledStatus());

                        String total = String.format("%.2f",Master.productTypeList.get(i).productItems.get(j).getUnitPrice()*Master.cartList.get(z).getQuantity());

                        Master.cartList.get(z).setTotal(Double.parseDouble(total));

                        dbHelper.updateProduct(
                                String.valueOf( Master.cartList.get(z).getUnitPrice()),
                                String.valueOf( Master.cartList.get(z).getQuantity()),
                                String.valueOf( Master.cartList.get(z).getTotal()),
                                String.valueOf( Master.cartList.get(z).getName()),
                                MemberDetails.getMobileNumber(),
                                MemberDetails.getSelectedOrgAbbr(),
                                String.valueOf( Master.cartList.get(z).getID()),
                                String.valueOf( Master.cartList.get(z).getImageUrl()),
                                String.valueOf(Master.cartList.get(z).getStockQuantity()),
                                Master.cartList.get(z).getStockEnabledStatus()
                        );
                    }
                }
            }
        }
    }
}
