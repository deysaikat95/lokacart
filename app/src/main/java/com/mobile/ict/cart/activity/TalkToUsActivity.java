package com.mobile.ict.cart.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Scroller;
import android.widget.Toast;

import com.alertdialogpro.AlertDialogPro;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.util.Validation;
import org.json.JSONException;
import org.json.JSONObject;

public class TalkToUsActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener{

    EditText eName, eEmail, eMobileNumber, eMessage,eAddress1,eAddress2;
    Button bSave;
    ImageView bFacebook,bWebsite;
    public static Boolean backPress;
    String url;
    Intent i;
    BottomSheetBehavior behavior;
    Button AboutUsButton, TncButton ,FbButton , FaqButton;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.talk_to_us_menu, menu);
        MenuItem infoMenuItem = menu.findItem(R.id.info);


        infoMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                if(behavior.getState() == 4){
                    behavior.setState(3);
                }else{
                    behavior.setState(4);
                }


                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        // React to state change

                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                        // React to dragging events
                    }
                });
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        backPress = false;
        setContentView(R.layout.activity_talk_to_us);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        View bottomSheet = findViewById(R.id.bottom_sheet);
        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setPeekHeight(2);
        AboutUsButton = (Button) bottomSheet.findViewById(R.id.button_info_about_us);
        TncButton = (Button) bottomSheet.findViewById(R.id.button_info_tnc);
        FbButton = (Button) bottomSheet.findViewById(R.id.button_info_fb);
        FaqButton = (Button) bottomSheet.findViewById(R.id.button_info_faq);

        AboutUsButton.setOnClickListener(this);
        TncButton.setOnClickListener(this);
        FbButton.setOnClickListener(this);
        FaqButton.setOnClickListener(this);

        setTitle(R.string.title_connect_with_us);

        eName = (EditText) findViewById(R.id.eEnquiryName);
        eName.setOnFocusChangeListener(this);

        eEmail = (EditText) findViewById(R.id.eEnquiryEmail);
        eEmail.setOnFocusChangeListener(this);

        eMobileNumber = (EditText) findViewById(R.id.eEnquiryMobileNumber);
        eMobileNumber.setOnFocusChangeListener(this);

        eAddress1 = (EditText) findViewById(R.id.eEnquiryAddress1);
        eAddress1.setOnFocusChangeListener(this);

        eAddress2 = (EditText) findViewById(R.id.eEnquiryAddress2);
        eAddress2.setOnFocusChangeListener(this);

        eMessage = (EditText) findViewById(R.id.eEnquiryMessage);
        eMessage.setScroller(new Scroller(TalkToUsActivity.this));
        eMessage.setVerticalScrollBarEnabled(true);
        eMessage.setMovementMethod(new ScrollingMovementMethod());
        eMessage.setOnFocusChangeListener(this);

        bSave = (Button) findViewById(R.id.bEnquirySave);
        bSave.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        backPress = false;
        behavior.setState(4);
        switch (v.getId())
        {
            case R.id.bEnquirySave:
                save();
                break;

            case R.id.button_info_fb:
                url = "https://www.facebook.com/RuralICT.iitb/";
                i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
            case R.id.button_info_about_us:
                i = new Intent(getApplicationContext(), InfoPage.class);
                i.putExtra("open", "about");
                startActivity(i);
                break;
            case R.id.button_info_tnc:
                i = new Intent(getApplicationContext(), InfoPage.class);
                i.putExtra("open", "tnc");
                startActivity(i);
                break;
            case R.id.button_info_faq:
                i = new Intent(getApplicationContext(), InfoPage.class);
                i.putExtra("open", "faq");
                startActivity(i);
                break;



        }
    }

    @Override
    public void onBackPressed() {
       super.onBackPressed();
       /* if(backPress)
            finish();
        else
        {
            backPress = true;
            Log.e("Enq form", "in else in onbackpressed");
            coordinatorLayout = (CoordinatorLayout) findViewById(R.id.enquiryCoordinatorLayout);
            Snackbar.make(coordinatorLayout, R.string.snackbar_press_back_once_more_to_exit, Snackbar.LENGTH_LONG).show();
        }*/
    }

    void save()
    {
        if(eName.getText().toString().trim().equals("")
                ||eEmail.getText().toString().trim().equals("")
                ||eMessage.getText().toString().trim().equals("")
                ||eAddress1.getText().toString().trim().equals("")
                ||eMobileNumber.getText().toString().trim().equals(""))
        {
            if(eName.getText().toString().trim().equals(""))
            {
                //Toast.makeText(this, R.string.toast_please_enter_name, Toast.LENGTH_SHORT).show();
                eName.setError(getString(R.string.error_required));
            }
            if(eEmail.getText().toString().trim().equals(""))
            {
               // Toast.makeText(this, R.string.toast_please_enter_email, Toast.LENGTH_SHORT).show();
                eEmail.setError(getString(R.string.error_required));
            }
            if(eMobileNumber.getText().toString().trim().equals(""))
            {
                //Toast.makeText(this, R.string.toast_please_enter_message, Toast.LENGTH_SHORT).show();
                eMobileNumber.setError(getString(R.string.error_required));
            }
            if(eAddress1.getText().toString().trim().equals(""))
            {
                // Toast.makeText(this, R.string.toast_please_enter_email, Toast.LENGTH_SHORT).show();
                eAddress1.setError(getString(R.string.error_required));
            }
            if(eMessage.getText().toString().trim().equals(""))
            {
                //Toast.makeText(this, R.string.toast_please_enter_message, Toast.LENGTH_SHORT).show();
                eMessage.setError(getString(R.string.error_required));
            }
        }
        else
        {
            if(eMobileNumber.getText().toString().trim().length()!=10)
            {
                Toast.makeText(getApplicationContext(), R.string.toast_enter_valid_number, Toast.LENGTH_LONG).show();
            }
            else if(eMobileNumber.getText().toString().trim().charAt(0) == '0')
            {
                Toast.makeText(getApplicationContext(), R.string.toast_dont_prefix_zero_to_the_mobile_number, Toast.LENGTH_LONG).show();
            }
            else if(!Validation.isValidEmail(eEmail.getText().toString().trim()))
            {
                Toast.makeText(getApplicationContext(), R.string.toast_enter_valid_emailid, Toast.LENGTH_LONG).show();
            }
            else
            {
                JSONObject enquiryObject = new JSONObject();
                try
                {
                    enquiryObject.put(Master.EMAIL, eEmail.getText().toString().trim());
                    enquiryObject.put("name", eName.getText().toString().trim());
                    enquiryObject.put("message",  eMessage.getText().toString().replaceAll("\\n", " "));
                    enquiryObject.put(Master.MOBILENUMBER, "91"+eMobileNumber.getText().toString().trim());
                    enquiryObject.put("address1", eAddress1.getText().toString().replaceAll("\\n", " "));
                    if(!eAddress2.getText().toString().trim().equals(""))
                        enquiryObject.put("address2", eAddress2.getText().toString().replaceAll("\\n", " "));

                    new EnquiryTask().execute(enquiryObject);
                }
                catch (JSONException e)
                {
                    Toast.makeText(TalkToUsActivity.this, R.string.alert_something_went_wrong, Toast.LENGTH_LONG).show();
                }
            }


        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
//        Log.e("Enq form", "in onFocusChange");
        if(hasFocus)
            backPress = false;
    }


    private class EnquiryTask extends AsyncTask<JSONObject, String, String>
    {
        @Override
        protected void onPreExecute() {
            Material.circularProgressDialog(TalkToUsActivity.this, getString(R.string.pd_sending_data_to_server), false);
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            Master.response = getJSON.getJSONFromUrl(Master.getEnquiryFormURL(), params[0], "POST", true,
                    MemberDetails.getEmail(), MemberDetails.getPassword());
//            System.out.println("Enquiry form response: " + Master.response);
            return Master.response;
        }

        @Override
        protected void onPostExecute(String s) {
            if(Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();
            try
            {
                Master.responseObject = new JSONObject(s);
                if (Master.responseObject.get("response").equals("Enquiry sent"))
                {
                    //Toast.makeText(TalkToUsActivity.this, getString(R.string.toast_the_enquiry_has_been_submitted), Toast.LENGTH_LONG).show();
                    final AlertDialogPro.Builder builder = new AlertDialogPro.Builder(TalkToUsActivity.this);
                    builder.setCancelable(false);
                    builder.setMessage(getString(R.string.toast_the_enquiry_has_been_submitted));
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            System.exit(0);
                        }
                    });
                    builder.show();
                }
                else if(Master.responseObject.get("response").equals("Failed to send enquiry"))
                {
                    Toast.makeText(TalkToUsActivity.this, getString(R.string.toast_unable_to_record_your_enquiry), Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(TalkToUsActivity.this, getString(R.string.toast_we_are_facing_some_technical_problems), Toast.LENGTH_LONG).show();
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }
    }
}
