package com.mobile.ict.cart.activity;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.flurry.android.FlurryAgent;
import com.mobile.ict.cart.R;
import com.mobile.ict.cart.container.MemberDetails;
import com.mobile.ict.cart.util.GetJSON;
import com.mobile.ict.cart.util.Master;
import com.mobile.ict.cart.util.Material;

import org.json.JSONException;
import org.json.JSONObject;

public class OrgInfoActivity extends AppCompatActivity {

    TextView tvInfoOrgName;
    TextView tvInfoOrgAbout;
    ImageView iVInfoOrgLogo;
    static String orgAbbr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_org_info);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.title_activity_Org_info));
        tvInfoOrgAbout = (TextView)findViewById(R.id.tvOrginfoabout);
        tvInfoOrgName = (TextView)findViewById(R.id.tvOrginfoName);
        iVInfoOrgLogo  = (ImageView)findViewById(R.id.ivOrginfoLogo);
        orgAbbr =  getIntent().getStringExtra("abbr");
        tvInfoOrgName.setText(getIntent().getStringExtra("name"));
        JSONObject params = new JSONObject();
        try {
            params.put("r","e");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        new getOrgInfo().execute(params);

    }


    public class getOrgInfo extends AsyncTask<JSONObject,String,String>{


        ProgressDialog pd;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            FlurryAgent.logEvent("getOrgInfoTask");
            Material.circularProgressDialog(OrgInfoActivity.this, getString(R.string.pd_terminating_your_membership), false);
        }

        @Override
        protected String doInBackground(JSONObject... params) {

            GetJSON getJson = new GetJSON();
            Log.d("orginfo url" , Master.getOrgInfoURL(orgAbbr));
            Master.response = getJson.getJSONFromUrl(Master.getOrgInfoURL(orgAbbr), null, "GET", true, MemberDetails.getEmail(), MemberDetails.getPassword());
            return Master.response;
        }

        @Override
        protected void onPostExecute(String response) {
            Log.d("orgInfo", response.toString());
            if(Material.circularProgressDialog.isShowing())
                Material.circularProgressDialog.dismiss();

            if(response.equals("exception")){



            }else{
                try {
                    JSONObject root = new JSONObject(response);
                    tvInfoOrgAbout.setText(root.getString("description"));
                    String url =  root.getString("logoUrl");
                    Glide.with(OrgInfoActivity.this).load(url)
                            .thumbnail(0.5f)
                            .crossFade()
                            .placeholder(getDrawable(R.drawable.profile_placeholder))
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into(iVInfoOrgLogo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
